from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'zad_dj_milo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'users.views.home', name='home'),
    url(r'^list_users/', 'users.views.list_users', name='list_users'),
    url(r'^view_user/', 'users.views.view_user', name='view_user'),
    url(r'^add_user/', 'users.views.add_user', name='add_user'),
    url(r'^edit_user/', 'users.views.edit_user', name='edit_user'),
    url(r'^delete_user/', 'users.views.delete_user', name='delete_user')
)
