from django import template
register = template.Library()
from datetime import date

@register.simple_tag(name="user_permission")
def user_permission(user):
	now_date = date.today()
	if abs((user.birthday_date - now_date).days) > 13 * 365:
		return "allowed"
	return "blocked"

@register.simple_tag(name="bizz_fuzz")
def bizz_fuzz(user):
	number = user.random_number
	return "%s%s" % ("Bizz" if number % 5 == 0 else "", "Fuzz" if number % 3 == 0 else "")
