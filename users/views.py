from django.shortcuts import render, get_object_or_404, redirect
from users.forms import UserProfileForm
from users.models import UserProfile

# Create your views here.
def home(request):
	return render(request, 'home.html')

def list_users(request):
	users = UserProfile.objects.all()
	return render(request, 'list_users.html', {'users': users})

def view_user(request):
	user = get_object_or_404(UserProfile, pk=request.GET.get('id', 0))
	return render(request, 'view_user.html', {'user': user})

def add_user(request):
	form = UserProfileForm()
	if request.method == "POST":
		user = UserProfileForm(request.POST)
		user.save()
		return redirect('/list_users/')
	return render(request, 'add_user.html', {'form': form})

def edit_user(request):
	user = get_object_or_404(UserProfile, pk=request.GET.get('id', 0))
	if request.method == "POST":
		user = UserProfileForm(request.POST, instance=user)
		user.save()
		return redirect('/list_users/')
	form = UserProfileForm(instance=user)
	return render(request, 'edit_user.html', {'form': form})

def delete_user(request):
	user = get_object_or_404(UserProfile, pk=request.GET.get('id', 0))
	user.delete()
	return redirect('/list_users/')	



