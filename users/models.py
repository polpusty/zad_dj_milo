from django.db import models
from random import randint
from django.contrib.auth.models import User

# Create your models here.
class UserProfile(models.Model):
	user = models.ForeignKey(User, unique=True)
	birthday_date = models.DateField()
	random_number = models.IntegerField(editable=False)

	def save(self, *args, **kwargs):
		self.random_number = randint(1, 100)
		super(UserProfile, self).save(*args, **kwargs)


